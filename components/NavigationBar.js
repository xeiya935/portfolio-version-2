import Link from 'next/link';
import { Navbar, Nav, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMicrochip, faLaptopCode, faGraduationCap, faBriefcase, faMobileAlt } from '@fortawesome/free-solid-svg-icons';

export default function NavigationBar() {
	return(
		<Navbar expand="lg" className="fixed-top" collapseOnSelect>
			<Navbar.Brand href="/" className="text-light">Arvin Lacdao</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" className="bg-light"/>
				<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Link href="/about">
						<a className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faMicrochip} />
							About Me
						</a>
					</Link>
					<Link href="/projects">
						<a className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faLaptopCode} />
							Projects
						</a>
					</Link>
					<Link href="/education">
						<a className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faGraduationCap} />
							Education
						</a>
					</Link>
					<Link href="/work">
						<a className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faBriefcase} />
							Work Experience
						</a>
					</Link>
					<Link href="/contact">
						<a className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faMobileAlt} />
							Contact Me
						</a>
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}