import React, { useEffect } from 'react';
import Head from 'next/head';
import { Container } from 'react-bootstrap';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavigationBar from '../components/NavigationBar';
import Footer from '../components/Footer';

function MyApp({ Component, pageProps }) {

	return (
		<React.Fragment>
			<Head>
				<script src="https://kit.fontawesome.com/8e97c30ac7.js" crossOrigin="anonymous"></script>
				<meta name="viewport" content="width=device-width, initial-scale=1" />
			</Head>
			<NavigationBar />
			<Component {...pageProps} />
			<Footer />
		</React.Fragment>
	)
	
}

export default MyApp
