import React from 'react';
import Head from 'next/head';
import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTint, faServer, faSave } from '@fortawesome/free-solid-svg-icons';

export default function About() {
	return(
		<React.Fragment>
			<Head>
				<title>About Me</title>
			</Head>
			<div className="background center flex-column page-container">
				<div className="header-container position-relative">
					<div className="center">
						<p className="header-glitch-1">About Me</p>
						<p className="header-glitch-2">About Me</p>
						<p className="header-glitch-3">About Me</p>
					</div>
					<hr className="header-hr about-hr"/>
				</div>
				<div className="position-relative">
					<div className="text-center">
						<div className="p-3">
							<p className="text-light text-center">
								I am a full stack web developer and educator by trade. I love to create programs that make work more efficient for everybody.
							</p>
							<p className="text-light text-center">
								Part of my goals is to learn and specialize on data science and robotics to help build a better future with equal opportunities for all.
							</p>
						</div>
					</div>
				</div>
				<div className="skills-section center">

					<div className="skills-container-lg center">

						<div className="skills-container-md center">

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">HTML</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faTint} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/html.png" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">CSS</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faTint} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/css.png" />
								</div>

							</div>

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">Bootstrap</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faTint} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/bootstrap.jpg" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">Javascript</p>
										<div className="skills-indicator-container center">
											<div className="skills-frontend center">
												<FontAwesomeIcon icon={faTint} color="white" />
											</div>
											<div className="skills-backend center">
												<FontAwesomeIcon icon={faServer} color="white" />
											</div>
										</div>
									</div>
									<img className="skills-img" src="/javascript2.png" />
								</div>

							</div>

						</div>

						<div className="skills-container-md center">

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">jQuery</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faTint} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/jquery.jpg" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">NodeJS</p>
										<div className="skills-indicator-container center">
											<div className="skills-backend center">
												<FontAwesomeIcon icon={faServer} color="white" />
											</div>
										</div>
									</div>
									<img className="skills-img" src="/nodejs.png" />
								</div>

							</div>

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">ExpressJS</p>
										<div className="skills-indicator-container center">
											<div className="skills-backend center">
												<FontAwesomeIcon icon={faServer} color="white" />
											</div>
										</div>
									</div>
									<img className="skills-img" src="/expressjs.png" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">MongoDB</p>
										<div className="skills-indicator-container center">
											<div className="skills-backend center">
												<FontAwesomeIcon icon={faServer} color="white" />
											</div>
										</div>
									</div>
									<img className="skills-img" src="/mongodb.jpg" />
								</div>

							</div>

						</div>

					</div>

					<div className="skills-container-lg center">

						<div className="skills-container-md center">

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">ReactJS</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faTint} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/reactjs.png" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">NextJS</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faTint} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/nextjs.png" />
								</div>

							</div>

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">Git</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faSave} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/git.png" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">GitLab</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faSave} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/gitlab.png" />
								</div>

							</div>

						</div>

						<div className="skills-container-md center">

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">Heroku</p>
										<div className="skills-frontend center">
											<FontAwesomeIcon icon={faSave} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/heroku.png" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">Postman</p>
										<div className="skills-backend center">
											<FontAwesomeIcon icon={faServer} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/postman.png" />
								</div>

							</div>

							<div className="center">

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">GraphQL</p>
										<div className="skills-backend center">
											<FontAwesomeIcon icon={faServer} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/graphql.png" />
								</div>

								<div className="skills-card center position-relative">
									<div className="skills-info center fixed-top position-absolute">
										<p className="skills-title flex-grow-1">SQL</p>
										<div className="skills-backend center">
											<FontAwesomeIcon icon={faServer} color="white" />
										</div>
									</div>
									<img className="skills-img" src="/sql.png" />
								</div>

							</div>

						</div>

					</div>

					<div className="center">
						<div className="skills-card center position-relative">
							<div className="skills-info center fixed-top position-absolute">
								<p className="skills-title flex-grow-1">PHP</p>
								<div className="skills-frontend center">
									<FontAwesomeIcon icon={faTint} color="white" />
								</div>
							</div>
							<img className="skills-img" src="/php.png" />
						</div>
						<div className="skills-card center position-relative">
							<div className="skills-info center fixed-top position-absolute">
								<p className="skills-title flex-grow-1">Laravel</p>
								<div className="skills-indicator-container center">
									<div className="skills-frontend center">
										<FontAwesomeIcon icon={faTint} color="white" />
									</div>
									<div className="skills-backend center">
										<FontAwesomeIcon icon={faServer} color="white" />
									</div>
								</div>
							</div>
							<img className="skills-img" src="/laravel.png" />
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}