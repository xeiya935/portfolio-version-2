import React from 'react';
import Head from 'next/head';

export default function Education() {
	return(
		<React.Fragment>
			<Head>
				<title>Education</title>
			</Head>
			<div className="background center flex-column page-container">
				<div className="header-container position-relative">
					<div className="center">
						<p className="header-glitch-1">Education</p>
						<p className="header-glitch-2">Education</p>
						<p className="header-glitch-3">Education</p>
					</div>
					<hr className="header-hr education-hr"/>
				</div>
				<div className="education-container pt-2 pl-5 pr-5">
					<div className="education-card text-center">
						<h4>Zuitt</h4>
						<p className="sub-header">Sen Gil J. Puyat Avenue, Makati City</p>
						<a href="https://zuitt.co/" target="_blank">
							<img className="card-img-thumbnail" src="/zuitt.png"/>
						</a>
						<h5 className="mt-3 mb-0">Web Development</h5>
						<p className="sub-header mt-1 m-0">Sep 2019 - Jan 2020</p>
					</div>
					<div className="education-card text-center">
						<h4>AICA</h4>
						<p className="sub-header">Capt. Henry P. Javier Street, Pasig City</p>
						<a href="https://aicaculinary.com/" target="_blank">
							<img className="card-img-thumbnail" src="/aica.png"/>
						</a>
						<h5 className="mt-3 mb-0">Diploma in Culinary Arts</h5>
						<p className="sub-header mt-1 m-0">Jul 2011 - Sep 2012</p>
					</div>
					<div className="education-card text-center">
						<h4>De La Salle University</h4>
						<p className="sub-header">Taft Avenue, Manila City</p>
						<a href="https://www.dlsu.edu.ph/" target="_blank">
							<img className="card-img-thumbnail" src="/dlsu.png"/>
						</a>
						<h5 className="mt-3 mb-0">Bachelor of Science in Accountancy</h5>
						<p className="sub-header mt-1 m-0">May 2007 - Oct 2008</p>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}