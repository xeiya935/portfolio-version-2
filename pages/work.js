import React from 'react';
import Head from 'next/head';

export default function Work() {
	return(
		<React.Fragment>
			<Head>
				<title>Work</title>
			</Head>
			<div className="background center flex-column page-container">
				<div className="header-container position-relative">
					<div className="center">
						<p className="header-glitch-1">Work Experience</p>
						<p className="header-glitch-2">Work Experience</p>
						<p className="header-glitch-3">Work Experience</p>
					</div>
					<hr className="header-hr work-hr"/>
				</div>
				<div className="works-container">
					<div className="works-card text-center">
						<h4>IT Instructor</h4>
						<h5>Zuitt</h5>
						<p className="sub-header m-0">Caswynn Building, Quezon City</p>
						<p className="sub-header">(Jan 2020 - Present)</p>
						<a href="https://zuitt.co/" target="_blank">
							<img className="card-img-thumbnail" src="/zuitt.png"/>
						</a>
						<p className="pt-3">
							Teaches students full stack web development to help them get into the web development industry.
						</p>
					</div>
					<div className="works-card text-center">
						<h4>Collections Associate</h4>
						<h5>HSBC</h5>
						<p className="sub-header m-0">Northgate Cyberzone, Muntinlupa City</p>
						<p className="sub-header">(Nov 2009 - May 2011)</p>
						<a href="https://www.hsbc.com.ph/" target="_blank">
							<img className="card-img-thumbnail" src="/hsbc.png"/>
						</a>
						<p className="pt-3">
							Completes outbound/outgoing calls to US/Canadian credit card holders to assist them with their financial status and to help maintain their credit. Assists card holders with finding available options for payment methods and providing the best solution for their payment needs.
						</p>
					</div>
					<div className="works-card text-center">
						<h4>Customer Service Representative</h4>
						<h5>Teleperformance</h5>
						<p className="sub-header m-0">Santana Grove, Paranaque City</p>
						<p className="sub-header">(Dec 2008 - Mar 2009)</p>
						<a href="https://www.teleperformance.com/en-us" target="_blank">
							<img className="card-img-thumbnail" src="/teleperformance.png"/>
						</a>
						<p className="pt-3">
							Completes inbound/incoming calls from US customers to assist them with hotel bookings. Assists customers with finding their desired hotel within their preferred location, amenities, etc.
						</p>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}